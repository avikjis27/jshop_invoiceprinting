/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import persistency.CustomerMaster;
import persistency.HibernateActivity;
import persistency.ItemMaster;
import persistency.SoldItemDetails;

/**
 *
 * @author IBM_ADMIN
 */
public class PopulateInvoice {
    String invoice_number;
    String customer_name;
    String customer_address;
    String customer_pin;
    String customer_city;
    String customer_state;
    String customer_contact_number;
    String total_bill_amount;
    ArrayList<SoldItemDetails> soldItems;
    public PopulateInvoice(long id,long custid){
        Session session=HibernateActivity.getHibernateSession();
        session.beginTransaction();
        Query query=session.createSQLQuery("select * from customer_master a where a.customer_id_l= :cust_id " ).addEntity(CustomerMaster.class);
        ArrayList<CustomerMaster> customer=(ArrayList<CustomerMaster>) query.setParameter("cust_id",custid).list();
        CustomerMaster customerMaster=customer.get(0);
        setCustomer_name(customerMaster.getCustomerName());
        setCustomer_address(customerMaster.getCustomerAddress());
        setCustomer_contact_number(customerMaster.getContact());
        setCustomer_pin(customerMaster.getPIN());
        setCustomer_city(customerMaster.getCity());
        setCustomer_state(customerMaster.getState());
        
        
        query=session.createSQLQuery("select * from sold_item_details a where a.bill_id= :bill_id" ).addEntity(SoldItemDetails.class);
        ArrayList<SoldItemDetails> items=(ArrayList<SoldItemDetails>) query.setParameter("bill_id",id).list();
        setSoldItems(items);
        
        query=session.createSQLQuery("select invoice_number_v, bill_amount_c from bill_details  where id_l= :bill_id" );
        
        query.setParameter("bill_id",id);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List results = query.list();
        for(Object object : results)
         {
            Map row = (Map)object;
            String invoiceNumber=row.get("INVOICE_NUMBER_V").toString();
            String  billAmount=row.get("BILL_AMOUNT_C").toString();
            setTotal_bill_amount(billAmount.toString());
            setInvoice_number(invoiceNumber.toString());
            
            
         }
        
        
        
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_pin() {
        return customer_pin;
    }

    public void setCustomer_pin(String customer_pin) {
        this.customer_pin = customer_pin;
    }

    public String getCustomer_city() {
        return customer_city;
    }

    public void setCustomer_city(String customer_city) {
        this.customer_city = customer_city;
    }

    public String getCustomer_state() {
        return customer_state;
    }

    public void setCustomer_state(String customer_state) {
        this.customer_state = customer_state;
    }
    

    public String getCustomer_contact_number() {
        return customer_contact_number;
    }

    public void setCustomer_contact_number(String customer_contact_number) {
        this.customer_contact_number = customer_contact_number;
    }

    public String getTotal_bill_amount() {
        return total_bill_amount;
    }

    public void setTotal_bill_amount(String total_bill_amount) {
        this.total_bill_amount = total_bill_amount;
    }

    public ArrayList<SoldItemDetails> getSoldItems() {
        return soldItems;
    }

    public void setSoldItems(ArrayList<SoldItemDetails> soldItems) {
        this.soldItems = soldItems;
    }
    
    
    
            
}
