package invoiceprinting;



import java.io.FileOutputStream;
import java.util.Calendar;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.PopulateInvoice;
import java.util.ArrayList;
import persistency.SoldItemDetails;


public class BillFormat {
	
	
        String filename= "C:/Invoice";
        private PopulateInvoice populateInvoice;
        //private long id;
        //private long custid;
        
public BillFormat(long id, long custid)
{
    //this.id=id;
    //this.custid=custid;
    populateInvoice=new PopulateInvoice(id, custid);
    filename=filename+"_"+populateInvoice.getInvoice_number()+"_"+timeStamp()+".pdf";
}
	
public String todaysDate()
{
	Calendar cal_date=Calendar.getInstance();
	String str_date="Date : "+cal_date.get(Calendar.DATE)+"/"+(cal_date.get(Calendar.MONTH)+1)+"/"+cal_date.get(Calendar.YEAR);
	
	return str_date;
}
public String timeStamp()
{
        Calendar cal_date=Calendar.getInstance();
	String str_date=cal_date.get(Calendar.DATE)+"_"+(cal_date.get(Calendar.MONTH)+1)+"_"+cal_date.get(Calendar.YEAR);
	
	return str_date;
}
	
	
 public void createPdf()
 {
	 
	 	Document document = new Document(PageSize.A3,400,400,550,500);
	 	 try 
	 	 {
			PdfWriter writer = PdfWriter.getInstance( document, new FileOutputStream( filename));
	
			document.open();
		
			pdfBillFomatting(document, writer);
		
			document.close();
	 	 
	 	 }
		
		catch (Exception e) 
	 	 {
			// TODO Auto-generated catch block
			e.printStackTrace();
	 	 } 
	 
 }
 
 
 public void createCustomerInfoTable(PdfContentByte canvas) throws DocumentException
 {
	 Font modFont=new Font(Font.FontFamily.COURIER,15,Font.BOLD);
	 
	 PdfPTable c_table = new PdfPTable(3);
	 c_table.setWidths(new int[]{30,30,30});
	 c_table.setTotalWidth(700f);
	 
	 PdfPCell c_cell = new PdfPCell(new Phrase("Customer Name  ",modFont));
	 c_cell.setLeading(20f,0f);
	 c_cell.setPaddingBottom(10f);
	 c_table.addCell(c_cell);
	 
	 PdfPCell cname_cell = new PdfPCell(new Phrase(populateInvoice.getCustomer_name(),modFont)); // Add Customer Name
	 cname_cell.setColspan(2);
	 c_table.addCell(cname_cell);
	 
	 PdfPCell address_cell = new PdfPCell(new Phrase("Customer Address  ",modFont));
	 address_cell.setPaddingBottom(20f);
	 c_table.addCell(address_cell);
	 
	 //Add Customer Address
	 
	 PdfPCell customeraddress_cell = new PdfPCell(new Phrase(populateInvoice.getCustomer_address(),modFont));
	 customeraddress_cell.setColspan(2);
	 c_table.addCell(customeraddress_cell);
	 
	 Phrase ph_city = new Phrase("City : ",modFont);
	 Phrase phcustomer_city = new Phrase(populateInvoice.getCustomer_city(),modFont); //Add City
	 Paragraph para_city = new Paragraph();
	 para_city.add(ph_city);
	 para_city.add(phcustomer_city);
	 PdfPCell city_cell = new PdfPCell(para_city);
	 city_cell.setPaddingBottom(10f);
	 c_table.addCell(city_cell);
	 
	 Phrase ph_pin = new Phrase("Pin : ",modFont);
	 Phrase phcustomer_pin = new Phrase(populateInvoice.getCustomer_pin(),modFont); // Add Pin
	 Paragraph para_pin = new Paragraph();
	 para_pin.add(ph_pin);
	 para_pin.add(phcustomer_pin);
	 PdfPCell pin_cell = new PdfPCell(para_pin);
	 pin_cell.setPaddingBottom(10f);
	 c_table.addCell(para_pin);

	
	 Phrase ph_state = new Phrase("State : ",modFont);
	 Phrase phcustomer_state = new Phrase(populateInvoice.getCustomer_state(),modFont); //Add State
	 Paragraph para_state = new Paragraph();
	 para_state.add(ph_state);
	 para_state.add(phcustomer_state);
	 PdfPCell state_cell = new PdfPCell(para_state);
	 state_cell.setPaddingBottom(10f);
	 c_table.addCell(state_cell);
	 
	 Phrase c_contactNo = new Phrase("Contact No ",modFont);
	 PdfPCell contact_cell = new PdfPCell(c_contactNo);
	 contact_cell.setPaddingBottom(10f);
	 c_table.addCell(contact_cell);
	 
	 Phrase add_contactNo = new Phrase(populateInvoice.getCustomer_contact_number(),modFont); //Add contact No
	 PdfPCell addcotact_cell = new PdfPCell(add_contactNo);
	 addcotact_cell.setColspan(2);
	 c_table.addCell(addcotact_cell);
	 
	 c_table.writeSelectedRows(0,-1, 70, 870, canvas);
	 
 }
 
 public void createItemDetailsTable(PdfContentByte canvas) throws DocumentException
 {
	 	Font headingFont=new Font(Font.FontFamily.COURIER,13,Font.BOLD);
	 	Font bigFont=new Font(Font.FontFamily.TIMES_ROMAN,15,Font.BOLD);
	 
	 	PdfPTable table = new PdfPTable(9);
		
		table.setWidths(new int[]{7,10,8,11,9,10,5,11,8});//10 2nd
		table.setTotalWidth(720f);
		
        table.setHorizontalAlignment(Element.ALIGN_CENTER);
     
		
		PdfPCell c1 = new PdfPCell(new Phrase("Sl No.",headingFont));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
		c1.setPaddingBottom(14f);
	    table.addCell(c1);
	    
	    
	    c1 = new PdfPCell(new Phrase("Product ID",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    
	    c1 = new PdfPCell(new Phrase("Type",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Manufacturer",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Model No.",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Sales Price",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Vat %",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Discount %",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Cost",headingFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(c1);
	   
	   try{
	    
	    int row=5;
            ArrayList<SoldItemDetails> soldItemDetailsArray=populateInvoice.getSoldItems();
	    
	    for (int i=0;i<soldItemDetailsArray.size();i++)
	    {
	    	SoldItemDetails soldItemDetails=soldItemDetailsArray.get(0);
                c1 = new PdfPCell(new Phrase(""+(i+1)));
	    	c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	c1.setPadding(7f);
	    	table.addCell(c1);
	    	
	    	PdfPCell cell_productId = new PdfPCell(new Phrase(soldItemDetails.getItemId())); //Add Product Id
	    	cell_productId.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_productId);
	    	
	    	PdfPCell cell_type  = new PdfPCell(new Phrase(soldItemDetails.getItemType())); // Add Product Type
	    	cell_type.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_type);
	    	
	    	PdfPCell cell_manufacturer  = new PdfPCell(new Phrase(soldItemDetails.getManufacturer())); //Add Manufacturer
	    	cell_manufacturer.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_manufacturer);	
	 	    
	    	PdfPCell cell_modelNo  = new PdfPCell(new Phrase(soldItemDetails.getItemModelNumber())); // Add Model No.
	    	cell_modelNo.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_modelNo);
	 	//String sellsPrice=soldItemDetails.getSellPrice();    
	    	PdfPCell cell_salesPrice  = new PdfPCell(new Phrase(Float.toString(soldItemDetails.getSellPrice()))); // Add Sales price
	    	cell_salesPrice.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_salesPrice);
	 	
                //System.out.println(soldItemDetails.getVat());
	    	PdfPCell cell_vat  = new PdfPCell(new Phrase(Float.toString(soldItemDetails.getVat())));	// Add VAT 
	    	cell_vat.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_vat);
	 	 
                
                //System.out.println(soldItemDetails.getDiscount());
	    	PdfPCell cell_discount  = new PdfPCell(new Phrase(Float.toString(soldItemDetails.getDiscount()))); // Add discount
	    	cell_discount.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_discount);
	 	   
	    	//System.out.println(soldItemDetails.getMRP());
                PdfPCell cell_cost  = new PdfPCell(new Phrase(Float.toString(soldItemDetails.getMRP()))); // Add Cost
	    	cell_cost.setHorizontalAlignment(Element.ALIGN_CENTER);
	    	table.addCell(cell_cost);
	    
	    }
	    
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	  
	    PdfPCell cell_total = new PdfPCell(new Phrase("Total Amount : ",bigFont));
	    cell_total.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    cell_total.setColspan(7);
	    cell_total.setPadding(6f);
	    table.addCell(cell_total);
	    
	    Chunk rs = new Chunk("Rs.  ",bigFont);
	    Chunk amount = new Chunk(populateInvoice.getTotal_bill_amount(),bigFont);  // Add Total Amount
	    Phrase rs_amount = new Phrase();
	    rs_amount.add(rs);
	    rs_amount.add(amount);
	    PdfPCell cell_totalAmount = new PdfPCell(rs_amount);
	    cell_totalAmount.setHorizontalAlignment(Element.ALIGN_CENTER);
	    cell_totalAmount.setColspan(2);
	    cell_totalAmount.setPadding(6f);
	    cell_totalAmount.setBackgroundColor(BaseColor.LIGHT_GRAY);
	    table.addCell(cell_totalAmount);
	    
	    table.writeSelectedRows(0,-1, 70,570, canvas); 
	    
 }
 
 
 
 
 
 public void pdfBillFomatting(Document document,PdfWriter writer)
 {
	 String imagePath ="C:/powerhouse.png";   // Change The Image Path accordingly
	
	 Font f_troman = new Font(Font.FontFamily.TIMES_ROMAN);
	 f_troman.setStyle("underline");
	 BaseFont bf_roman= f_troman.getCalculatedBaseFont(false);
	 
	 
	 Font f_broman = new Font(Font.FontFamily.TIMES_ROMAN);
	 f_broman.setStyle("bold");
	 BaseFont bf_broman= f_broman.getCalculatedBaseFont(false);

	try 
	{
		
		PdfContentByte canvas = writer.getDirectContentUnder();
		canvas.saveState();
		canvas.beginText();
		canvas.setFontAndSize(bf_roman,30);
		canvas.showTextAligned(Element.ALIGN_LEFT,"TARAMA",220, 1088, 0);
		canvas.showTextAligned(Element.ALIGN_LEFT,"P",370, 1088, 0);
		
		Image image = Image.getInstance(imagePath);
		image.setAbsolutePosition(373f,1070f);
		document.add(image);
		
		canvas.showTextAligned(Element.ALIGN_LEFT,"WER HOUSE",454, 1088, 0);
		
		canvas.setFontAndSize(bf_roman,14);
		canvas.showTextAligned(Element.ALIGN_LEFT,"( Kodalia More ,G.T.Road, Bandel ,Hooghly )",280, 1045, 0);
		
		canvas.setFontAndSize(bf_broman,13);
		canvas.showTextAligned(Element.ALIGN_LEFT,"Shop(Ph No.):2631-0917",650, 1168, 0);
		canvas.showTextAligned(Element.ALIGN_LEFT,"Mob No   :   9433541157",650, 1152, 0);
		
		canvas.showTextAligned(Element.ALIGN_LEFT,todaysDate(),18, 1168, 0);
                
                canvas.setFontAndSize(bf_broman, 14);
		canvas.showTextAligned(Element.ALIGN_LEFT,"Invoice No. : " ,18,1140,0);
		canvas.showTextAligned(Element.ALIGN_LEFT,populateInvoice.getInvoice_number() ,100,1140,0);         // Add INVOICE NO. from DB
		
		canvas.setFontAndSize(bf_broman,16);
		canvas.showTextAligned(Element.ALIGN_LEFT,"Customer Details :",30, 930, 0);
		
		canvas.setFontAndSize(bf_broman,16);
		canvas.showTextAligned(Element.ALIGN_LEFT,"Item Details :",30, 630, 0);
		
		canvas.showTextAligned(Element.ALIGN_LEFT,"----------------------------------------------",530,70, 0);
		canvas.showTextAligned(Element.ALIGN_LEFT,"signature",630,58, 0);
		
		createCustomerInfoTable(canvas);  // Calling createCustomerInfoTable method
		createItemDetailsTable(canvas);
		
				
		System.out.println("end of item details");
		
		canvas.endText();
		canvas.restoreState();
		
		
		
	}
	catch (Exception e) 
	{
		
		e.printStackTrace();
	}
	
	 
 }
	
	
	
/*public static void main(String[] args) 
{
		
		// TODO Auto-generated method stub
 
	BillFormat billformat =new BillFormat();
	
	billformat.createPdf();

	System.out.println("Path of PDF : "+filename);

}*/


}
