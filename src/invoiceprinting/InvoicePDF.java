package invoiceprinting;

//package invoiceprinting;
import java.io.FileOutputStream;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.util.Iterator;
import java.util.Set;
import persistency.BillDetails;
import persistency.SoldItemDetails;

public class InvoicePDF {

	public static String filename;
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18);
	private static Font modFont=new Font(Font.FontFamily.COURIER,12,Font.BOLD);
	private static Font headlineFont=new Font(Font.FontFamily.TIMES_ROMAN,12,Font.BOLD);
        BillDetails billDetails;
	
	public void generateInvoice(BillDetails billDetails ) {
		
		// TODO Auto-generated method stub
                this.billDetails=billDetails;
                filename="C:/invoice/invoice"+Float.toString(billDetails.getId())+".pdf";
                
		try
		{
			// You can check with A4 size too.
		Document document = new Document(PageSize.A3, 20, 20, 20, 20); 
		
		PdfWriter.getInstance( document, new FileOutputStream( filename) );
		
		document.open();
		
		//Set one or more than one Font style together
		catFont.setStyle("bold,underline");
		headlineFont.setStyle("bold,underline");
		
		
		//Title
		Paragraph paraTitle= new Paragraph("Tarama Power House",catFont);
		paraTitle.setAlignment(Element.ALIGN_CENTER);
		document.add(paraTitle);
		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		//Customer Name
		Paragraph paraCname = new Paragraph();
		paraCname.setFont(modFont);
		paraCname.add("Customer Name :");
		paraCname.setAlignment(Element.ALIGN_LEFT);
		document.add(paraCname);
		
		document.add(Chunk.NEWLINE); // create blank line
		
		//Customer Address
		Paragraph paraAddress=new Paragraph("Customer Address :",modFont);
		paraAddress.setAlignment(Element.ALIGN_LEFT);
		document.add(paraAddress);
		
		
		document.add(Chunk.NEWLINE); 
		document.add(Chunk.NEWLINE);
		
		
		//adding City,Pin and state at same Line
		Chunk city= new Chunk("City :",modFont);
		
        Chunk pin= new Chunk("Pin No. :",modFont);
        
        Chunk state= new Chunk("State :",modFont);
        
        InvoicePDF xp=new InvoicePDF();
        
        Phrase phr=new Phrase();
        
        phr.add(city);
        xp.addBlankSpace(40, phr);
        phr.add(pin);                 
        xp.addBlankSpace(40, phr);
        phr.add(state);
        
        document.add(phr);
        
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        
        //Contact No.
        Paragraph paraContact = new Paragraph("Contact No. :",modFont);
        document.add(paraContact);
        
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        
        //Item Details
        
        Paragraph paraItem =new Paragraph("Item Details ",headlineFont);
        paraItem.setAlignment(Element.ALIGN_LEFT);
        document.add(paraItem);
        
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
                
        
        // Table creation method call and method return a table type obj.
        
        PdfPTable table= createTable(billDetails.getItems().size());
        addData(table);
        
        document.add(table);
    
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        
        Chunk total = new Chunk("Total Amount :Rs. "+billDetails.getBillAmount(),headlineFont);
        Phrase phr_total= new Phrase();
        
        xp.addBlankSpace(110, phr_total);
        phr_total.add(total);
        
        Paragraph para_total = new Paragraph();
        para_total.add(phr_total);
        para_total.setAlignment(Element.ALIGN_LEFT);
        		
		document.add(para_total);
		
		
		document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        
		
		Paragraph para_dotLine = new Paragraph("---------------------------------",modFont);
		para_dotLine.setAlignment(Element.ALIGN_RIGHT);
		document.add(para_dotLine);
		
		Paragraph para_signature = new Paragraph("Signature             ",modFont);
		para_signature.setAlignment(Element.ALIGN_RIGHT);
		
		document.add(para_signature);

		document.close();
		
		System.out.println("Path of PDF : "+filename);
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		

	}
	
	// adding blanks space between two words
	public void addBlankSpace(int noOfBlank,Phrase phr)
	{
		for(int i=0; i<=noOfBlank; i++) {
                phr.add(" ");
            }
	}

	
	//Method for creating a table 
	
	public PdfPTable createTable( int row) throws BadElementException
	{
		PdfPTable table = new PdfPTable(9);
		
		int[] widths = {9, 150};
        table.setHorizontalAlignment(Element.ALIGN_LEFT);
        
		
		PdfPCell c1 = new PdfPCell(new Phrase("Sl No.",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    c1.setSpaceCharRatio(5/800f);
	    table.addCell(c1);
	    
	    
	    c1 = new PdfPCell(new Phrase("Product ID",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    
	    c1 = new PdfPCell(new Phrase("Type",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Manufacturer",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Model No.",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Sales Price",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Vat %",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Discount %",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    c1 = new PdfPCell(new Phrase("Cost",modFont));
	    c1.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table.addCell(c1);
	    
	    
	    
	    
	    return table;
	}
        
        public void addData(PdfPTable table)
        {   Set<SoldItemDetails> set=billDetails.getItems();
            Iterator iterator=set.iterator();
            for (int i=0;iterator.hasNext();i++)
	    {   SoldItemDetails soldItemDetails=(SoldItemDetails) iterator.next();
	    	table.addCell(""+(i+1));
	    	table.addCell(soldItemDetails.getItemId());          // we have to comlete all data of a row 
	 	table.addCell(soldItemDetails.getItemType());			 // otherwise row will not come to in picture
	 	table.addCell(soldItemDetails.getManufacturer());
	 	table.addCell(soldItemDetails.getItemModelNumber());
	 	table.addCell(Float.toString(soldItemDetails.getSellPrice()));
	 	table.addCell(Float.toString(soldItemDetails.getVat()));
	 	table.addCell(Float.toString(soldItemDetails.getDiscount()));
	 	table.addCell("");
	 	    
	      System.out.println("Row Value : "+i);
	      
	    }
        }
	
}
